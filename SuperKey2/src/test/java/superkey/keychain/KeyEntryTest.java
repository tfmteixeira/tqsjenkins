/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static superkey.keychain.KeyEntry.FIELDS_DELIMITER;

/**
 *
 * @author ico
 */
public class KeyEntryTest {

    private KeyEntry entryA, entryEmpty;

    public KeyEntryTest() {
    }

    @Before
    public void setUp() {
        entryA = new KeyEntry();
        entryA.setApplicationName("appx");
        entryA.setUsername("xx");
        entryA.setPassword("secret@@@");

    }

    @After
    public void tearDown() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetApplicationNameWithNull() {
        entryA.setApplicationName(null);
    }

    @Test
    public void testKey() {
        // the key is the application name
        assertEquals("failed to get existing key field", entryA.getApplicationName(), "appx");
    }

    @Test
    public void testFormatAsCsv() {
        String expects = "appx" + KeyEntry.FIELDS_DELIMITER + "xx" + KeyEntry.FIELDS_DELIMITER + "secret@@@";
        assertEquals("failed to format entry to delimited string", entryA.formatAsCsv(), expects);

    }

    @Test
    public void testToString() {
        String expects = "appx" + "\t" + "xx" + "\t" + "secret@@@";
        StringBuilder builder = new StringBuilder();
        builder.append(entryA.getApplicationName());
        builder.append("\t");
        builder.append(entryA.getUsername());
        builder.append("\t");
        builder.append(entryA.getPassword());
        assertEquals("failed to write toString()", builder.toString(), expects);
    }

    @Test
    public void testParse() {
        entryEmpty = entryA.parse("appx" + KeyEntry.FIELDS_DELIMITER + "xx" + KeyEntry.FIELDS_DELIMITER + "secret@@@");

        assertEquals("failed to parse Aplication Namee", entryEmpty.getApplicationName(), "appx");
        assertEquals("failed to parse Username", entryEmpty.getUsername(), "xx");
        assertEquals("failed to parse Password", entryEmpty.getPassword(), "secret@@@");
    }

}
